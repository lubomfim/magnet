import React from 'react';

import './styles.css';

export const Benefits = () => {
  return (
    <div className="benefits">
      <div className="benefits__wrapper">
        <h3 className="benefits__title">Você vai se surpreender</h3>
        <ul className="benefits__list">
          <li className="benefits__item">
            <p className="benefits__item-title">Atendimento exclusivo</p>
            <p className="benefits__item-text">
              Nos preocupamos verdadeiramente com o sucesso da sua empresa. Vamos entender suas necessidades e desenhar
              a melhor solução para você.
            </p>
          </li>
          <li className="benefits__item">
            <p className="benefits__item-title">Time to value</p>
            <p className="benefits__item-text">
              Você e seu time vai conseguir perceber os resultados gerados pela implantação da Plataforma Magnet
              Customer em pouquíssimo tempo.
            </p>
          </li>
          <li className="benefits__item">
            <p className="benefits__item-title">Constante evolução</p>
            <p className="benefits__item-text">
              Nosso time de produto está atento a todas as oportunidades de melhorias apontadas pelos nossos clientes e
              colaboradores. Toda semana uma novidade.
            </p>
          </li>
          <li className="benefits__item">
            <p className="benefits__item-title">Conexão total</p>
            <p className="benefits__item-text">
              Fique tranquilo, vamos fazer todas integrações necessárias para que você e sua empresa consigam ter a
              melhor experiência com nossa Plataforma de CRM
            </p>
          </li>
        </ul>
      </div>
    </div>
  );
};
