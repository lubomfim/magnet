import React from 'react';
import { Link } from 'react-router-dom';
import PropType from 'prop-types';

import './styles.css';

export const CTA = ({ fontSize, width, padding, text, type }) => {
  const newStyle = {
    fontSize: fontSize,

    width: width,
    padding: padding,
  };

  const link = type ? `/${type}` : '/onboarding/0';

  return !type ? (
    <Link to={link} className="cta__button" style={newStyle}>
      {text}
    </Link>
  ) : (
    <a className="cta__button" style={newStyle} href="https://mkt.magnetcustomer.com/fale-com-o-especialista">
      {text}
    </a>
  );
};

CTA.propTypes = {
  fontSize: PropType.string,
  width: PropType.string,
  padding: PropType.string,
  text: PropType.string.isRequired,
  type: PropType.string,
};
