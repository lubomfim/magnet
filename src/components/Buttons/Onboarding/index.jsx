import React from 'react';
import PropType from 'prop-types';

export const OnboardingBtn = ({ handleNext, text }) => {
  return (
    <div className="onboarding__btn-wrapper">
      <button className="onboarding__btn" onClick={handleNext}>
        {text}
      </button>
    </div>
  );
};

OnboardingBtn.propTypes = {
  handleNext: PropType.func.isRequired,
  text: PropType.string.isRequired,
};
