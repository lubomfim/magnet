import React, { useEffect, useRef, useState } from 'react';
import { saveToLocalStorage, loadFromLocalstorage } from 'utils/localStorage';

import './styles.css';

const Cookies = () => {
  const [accept, setAccept] = useState(false);
  const cookie = useRef();
  const acceptCookies = () => {
    saveToLocalStorage('cookies', true);
    cookie.current.style.bottom = '-200px';
    setTimeout(() => {
      setAccept(true);
    }, 1000);
  };

  useEffect(() => {
    if (loadFromLocalstorage('cookies')) {
      setAccept(true);
    }
  }, []);

  return (
    !accept && (
      <div className="cookies" ref={cookie}>
        <p className="cookies__message">Este site usa cookies para melhorar a experiência do usuário.</p>
        <button className="cookies__button" onClick={() => acceptCookies()}>
          Entendi
        </button>
      </div>
    )
  );
};

export default Cookies;
