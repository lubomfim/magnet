import React from 'react';

import './styles.css';

const questions = [
  {
    question: 'Qual a diferença do Módulo comercial para o CRM completo?',
    answer:
      'O Módulo comercial foi desenvolvido para que as empresas possam ter uma gestão comercial organizada e com uma visualização mais amigável. Já com o CRM completo você vai contar com uma Plataforma completa de construção de relacionamento com clientes (Vendas ao Pós-venda), tudo integrado.',
  },
  {
    question: 'Posso contratar o Modulo comercial e depois migrar para o CRM completo?',
    answer: `
      Sim, você pode.
      O nosso objetivo ao separa esses dois produtos, foi para facilitar a vida do
      pequeno empreender que deseja iniciar organizar seus processos
      comerciais. Ou grandes empresas que estão ajustando sua operação
      comercial e querem experimentar uma ferramenta que melhor se adeque a
      suas necessidades.
      `,
  },
  {
    question: 'Tenho que pagar setup?',
    answer:
      'O Setup é paga quando uma empresa contrata o CRM Completo. Entendemos que implantar uma Plataforma de CRM é uma avanço importante que a empresa está dando, por isso temos os profissionais mais experientes e qualificados para fazer essa jornada ser positiva e capaz de gerar os resultados esperados.',
  },
  {
    question: 'Qual o tempo mínimo de contrato?',
    answer:
      'Quando você contrata o módulo comercial, você está realizando a assinatura do produto, logo não existe tempo mínimo ou máximo. Você pode cancelar a assinatura a qualquer momento. Quando você contrata o CRM completo, você está adquirindo uma licença de uso do software por 12 meses, porem paga o valor mensalmente por essa contratação.',
  },
];

export const FAQ = () => {
  const handleClick = (index) => {
    const element = document.querySelector(`.faq__item${index}`);
    const img = document.querySelector(`.faq__item-img${index}`);

    element.classList.toggle('faq__item-active');
    img.classList.toggle('faq__item-img--reverse');
  };

  return (
    <div className="faq">
      <div className="faq__wrapper">
        <div className="faq__title">Perguntas Frequentes</div>
        <ul className="faq__list">
          {questions.map((el, index) => {
            const titleLength =
              el.question.length > 40
                ? 'faq__question-large'
                : el.question.length > 30
                ? 'faq__question-medium'
                : 'faq__question-small';

            return (
              <li
                key={index}
                className={`${titleLength} faq__item faq__item${index} ${index === 0 ? 'faq__item-active' : ''}`}
                onClick={() => handleClick(index)}
              >
                <div className={`faq__question-wrapper`}>
                  <p className="faq__question">{el.question}</p>
                  <img
                    src="/images/icons/dropdown.svg"
                    alt="Dropdown icon"
                    className={`faq__item-img faq__item-img${index}`}
                  />
                </div>
                <div className={`faq__answer`}>{el.answer}</div>
              </li>
            );
          })}
        </ul>
      </div>
    </div>
  );
};
