import React from 'react';

import './styles.css';

import { CTA } from '../Buttons/CTA/';
import { Link } from 'react-router-dom';

export const Footer = () => {
  return (
    <footer className="footer">
      <div className="container">
        <div className="footer__wrapper">
          <div className="footer__top">
            <h2 className="footer__title">
              Nosso time está a disposição para te ajudar a construir relacionamento com seus clientes e gerar mais
              negócios para sua empresa.
            </h2>
            <CTA text="Solicitar Contato" fontSize="18px" type="especialista" />
          </div>
          <div className="footer__bottom">
            <div className="footer__logo">
              <img src="/images/logos/logo-branco.png" alt="Magnet Customer" />
            </div>
            <div className="footer__menu">
              <p className="footer__menu-title">Como podemos te auxiliar?</p>
              <Link to="/sobre" className="footer__menu-item">
                Sobre Magnet Customer
              </Link>
              <Link to="/blog/oqueecrm" className="footer__menu-item">
                O que é CRM?
              </Link>
              <a
                href="https://www.linkedin.com/company/magnet-customer/jobs/"
                className="footer__menu-item"
                target="_blank"
                rel="noreferrer"
              >
                Trabalhe Conosco
              </a>
              {/*  <Link className="footer__menu-item">Magnet Customer</Link>
              <Link className="footer__menu-item">Conheça o cliente</Link> */}
            </div>
            <div className="footer__menu">
              <p className="footer__menu-title">Menu navegação</p>
              <Link to="/home" className="footer__menu-item">
                Home
              </Link>

              <Link to="/planos" className="footer__menu-item">
                Planos
              </Link>
              <Link to="/solucoes/funcao#vendas" className="footer__menu-item">
                Soluções
              </Link>
              <Link to="/contato" className="footer__menu-item">
                Contato
              </Link>
              <a
                href="https://direcionamento-login-magnetcustomer.s3.amazonaws.com/index.html"
                className="footer__menu-item"
              >
                Entrar
              </a>
              <CTA text="Experimente!" />
            </div>
          </div>
        </div>
      </div>
      <small className="copyright">Copyright © 2021 Magnet Customer. Todos os direitos reservados.</small>
    </footer>
  );
};
