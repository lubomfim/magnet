import React from 'react';
import { HashLink as Link } from 'react-router-hash-link';
import PropTypes from 'prop-types';

export const DropdownProductsServices = ({ handleClick }) => {
  const scrollWithOffset = (el) => {
    const yCoordinate = el.getBoundingClientRect().top + window.pageYOffset;
    const yOffset = -100;
    window.scrollTo({ top: yCoordinate + yOffset, behavior: 'smooth' });
  };

  return (
    <React.Fragment>
      <div className="dropdown__item">
        <ul className="dropdown__list">
          <li className="dropdown__list-subtitle">Produtos</li>
          <li className="dropdown__list-link">
            <Link scroll={(el) => scrollWithOffset(el)} to="/solucoes/funcao#vendas" onClick={() => handleClick()}>
              Módulo Comercial
            </Link>
          </li>
          <li className="dropdown__list-link">
            <Link scroll={(el) => scrollWithOffset(el)} to="/solucoes/funcao#solucao" onClick={() => handleClick()}>
              Solução completa de CRM
            </Link>
          </li>
        </ul>
      </div>
      {/*    <div className="dropdown__item">
        <ul className="dropdown__list">
          <li className="dropdown__list-subtitle">Serviços</li>
          <li className="dropdown__list-link">
            <Link scroll={(el) => scrollWithOffset(el)} to="/" onClick={() => handleClick()}>
              Consultoria
            </Link>
          </li>
          <li className="dropdown__list-link">
            <Link scroll={(el) => scrollWithOffset(el)} to="/" onClick={() => handleClick()}>
              Terceirização
            </Link>
          </li>
          <li className="dropdown__list-link">
            <Link scroll={(el) => scrollWithOffset(el)} to="/" onClick={() => handleClick()}>
              Treinamento
            </Link>
          </li>
        </ul>
      </div> */}
    </React.Fragment>
  );
};

DropdownProductsServices.propTypes = {
  handleClick: PropTypes.func.isRequired,
};
