import React from 'react';
import { HashLink as Link } from 'react-router-hash-link';
import PropTypes from 'prop-types';

export const DropdownSolution = ({ handleClick }) => {
  const scrollWithOffset = (el) => {
    const yCoordinate = el.getBoundingClientRect().top + window.pageYOffset;
    const yOffset = -100;
    window.scrollTo({ top: yCoordinate + yOffset, behavior: 'smooth' });
  };

  return (
    <React.Fragment>
      <div className="dropdown__item-wrapper">
        <div className="dropdown__item">
          <ul className="dropdown__list">
            <li className="dropdown__list-subtitle">Por função</li>
            <li className="dropdown__list-link">
              <Link scroll={(el) => scrollWithOffset(el)} onClick={() => handleClick()} to="/solucoes/funcao#vendas">
                Vendas
              </Link>
            </li>
            <li className="dropdown__list-link">
              <Link scroll={(el) => scrollWithOffset(el)} onClick={() => handleClick()} to="/solucoes/funcao#marketing">
                Marketing de Relacionamento
              </Link>
            </li>
            <li className="dropdown__list-link">
              <Link
                scroll={(el) => scrollWithOffset(el)}
                onClick={() => handleClick()}
                to="/solucoes/funcao#atendimento"
              >
                Atendimento
              </Link>
            </li>
            <li className="dropdown__list-link">
              <Link scroll={(el) => scrollWithOffset(el)} onClick={() => handleClick()} to="/solucoes/funcao#solucao">
                Solução completa
              </Link>
            </li>
          </ul>
          <ul className="dropdown__list">
            <li className="dropdown__list-subtitle">Por tamanho</li>
            <li className="dropdown__list-link">
              <Link scroll={(el) => scrollWithOffset(el)} onClick={() => handleClick()} to="/solucoes/tamanho#pequena">
                Para pequenas empresas
              </Link>
            </li>
            <li className="dropdown__list-link">
              <Link scroll={(el) => scrollWithOffset(el)} onClick={() => handleClick()} to="/solucoes/tamanho#grande">
                Para grandes empresas
              </Link>
            </li>
          </ul>
        </div>
        <div className="dropdown__item">
          <ul className="dropdown__list">
            <li className="dropdown__list-subtitle">Por segmento</li>
            <li className="dropdown__list-link">
              <Link
                scroll={(el) => scrollWithOffset(el)}
                onClick={() => handleClick()}
                to="/solucoes/segmento#educacao"
              >
                Educação
              </Link>
            </li>
            <li className="dropdown__list-link">
              <Link
                scroll={(el) => scrollWithOffset(el)}
                onClick={() => handleClick()}
                to="/solucoes/segmento#automotivo"
              >
                Automotivo
              </Link>
            </li>
            <li className="dropdown__list-link">
              <Link
                scroll={(el) => scrollWithOffset(el)}
                onClick={() => handleClick()}
                to="/solucoes/segmento#servicos-financeiros"
              >
                Serviços financeiros
              </Link>
            </li>

            <li className="dropdown__list-link">
              <Link
                scroll={(el) => scrollWithOffset(el)}
                onClick={() => handleClick()}
                to="/solucoes/segmento#startups"
              >
                Startups
              </Link>
            </li>
          </ul>
        </div>
      </div>
      <div className="dropdown__additional">
        <Link to="/contato" className="dropdown__additional-link">
          Seja um parceiro Magnet Customer!
        </Link>
      </div>
    </React.Fragment>
  );
};

DropdownSolution.propTypes = {
  handleClick: PropTypes.func.isRequired,
};
