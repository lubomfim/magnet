import React from 'react';
import PropTypes from 'prop-types';

import { DropdownProductsServices } from './ProductService';
import { DropdownSolution } from './Solution';

import './styles.css';

export const Dropdowns = ({ type, toggleDropdown, toggleBurguer }) => {
  const handleClick = () => {
    toggleBurguer();
    toggleDropdown(false);
  };
  return (
    <div className="dropdown__wrapper">
      <div className="dropdown__content">
        <div className="dropdown__back">
          <p className="dropdown__back-text" onClick={() => toggleDropdown(false)}>
            Voltar
          </p>
        </div>
        {type === 'productService' && <DropdownProductsServices handleClick={handleClick} />}
        {type === 'solution' && <DropdownSolution handleClick={handleClick} />}
      </div>
    </div>
  );
};

Dropdowns.propTypes = {
  type: PropTypes.string.isRequired,
  toggleDropdown: PropTypes.func.isRequired,
  toggleBurguer: PropTypes.func.isRequired,
};
