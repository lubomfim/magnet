import React, { useRef, useState } from 'react';
import { Link } from 'react-router-dom';
import './styles.css';

import { CTA } from '../Buttons/CTA/';
import { Dropdowns } from './Dropdowns';

export const Header = () => {
  const [solucoesDropdown, setSolucoesDropdown] = useState(false);
  const [productServiceDropdown, setProductServiceDropdown] = useState(false);
  const menu = useRef();

  const toggleBurguer = () => {
    const menuCurrent = menu.current;
    const screenSize = screen.width;

    menuCurrent.classList.toggle('show');

    if (screenSize > 1000) {
      return (document.body.style.overflowY = 'scroll');
    } else {
      if (menuCurrent.classList.value.includes('show')) {
        document.body.style.overflowY = 'hidden';
      } else {
        document.body.style.overflowY = 'scroll';
      }
    }
  };

  return (
    <header className="header">
      <div className="container">
        <div className="header__wrapper">
          <Link to="/home" className="header__logo-wrapper">
            <img src="/images/logos/logo.png" alt="Magnet Customer Logo" />
          </Link>
          <div className="header__burguer-menu" onClick={() => toggleBurguer()}>
            <div className="header__burguer-line"></div>
            <div className="header__burguer-line"></div>
            <div className="header__burguer-line last-line"></div>
          </div>
          <nav className="header__nav" ref={menu}>
            <div className="header__nav-burguer" onClick={() => toggleBurguer()}>
              <img src="/images/icons/cancel.svg" alt="Close Button" />
            </div>
            <ul className="header__nav-list">
              <li className="header__nav-item">
                <Link to="/sobre" className="header__nav-link" onClick={() => toggleBurguer()}>
                  A Magnet Customer
                </Link>
              </li>

              <li
                className="header__nav-dropdown"
                onMouseEnter={() => setSolucoesDropdown(true)}
                onMouseLeave={() => setSolucoesDropdown(false)}
              >
                <div className="nav-dropdown__item  header__nav-item" onClick={() => setSolucoesDropdown(true)}>
                  <p className="header__nav-link" onClick={() => setSolucoesDropdown(true)}>
                    Soluções
                  </p>
                  <img src="/images/icons/dropdown.svg" alt="Dropdown icon" className="nav__dropdown-icon" />
                </div>
                {solucoesDropdown && (
                  <Dropdowns type="solution" toggleDropdown={setSolucoesDropdown} toggleBurguer={toggleBurguer} />
                )}
              </li>
              <li
                className="header__nav-dropdown"
                onMouseEnter={() => setProductServiceDropdown(true)}
                onMouseLeave={() => setProductServiceDropdown(false)}
              >
                <div className="nav-dropdown__item header__nav-item" onClick={() => setProductServiceDropdown(true)}>
                  <p className="header__nav-link">Produtos</p>
                  <img src="/images/icons/dropdown.svg" alt="Dropdown icon" className="nav__dropdown-icon" />
                </div>
                {productServiceDropdown && (
                  <Dropdowns
                    type="productService"
                    toggleDropdown={setProductServiceDropdown}
                    toggleBurguer={toggleBurguer}
                  />
                )}
              </li>
              <li className="header__nav-item">
                {/*   <Link to="/contato" className="header__nav-link" onClick={() => toggleBurguer()}>
                  Contato
                </Link> */}
                <Link to="/planos" className="header__nav-link" onClick={() => toggleBurguer()}>
                  Planos
                </Link>
              </li>
              <li className="header__nav-item">
                <a href="https://pages.magnetcustomer.com" className="header__nav-link">
                  Entrar
                </a>
              </li>
              <li className="header__nav-item" style={{ marginRight: '0' }}>
                <CTA text="Fale com o consultor" fontSize="14px" type="especialista" />
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </header>
  );
};
