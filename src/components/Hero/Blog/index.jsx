import React from 'react';
import { CTA } from '../../Buttons/CTA/';
import PropType from 'prop-types';

import './styles.css';

export const BlogHeader = ({ title, illustration, description, cta }) => {
  return (
    <div className="blog-header">
      <div className="container">
        <div className="blog-header__wrapper">
          <div className="blog-header__text">
            <h1 className="blog-header__title">{title}</h1>
            <p className="blog-header__description">{description}</p>
            <CTA text={cta} />
          </div>
          <div className="blog-header__illustration-wrapper">
            <img src={`/images/blog/${illustration}.svg`} alt={illustration} />
          </div>
        </div>
      </div>
    </div>
  );
};

BlogHeader.propTypes = {
  title: PropType.string.isRequired,
  illustration: PropType.string.isRequired,
  description: PropType.string.isRequired,
  cta: PropType.string.isRequired,
};
