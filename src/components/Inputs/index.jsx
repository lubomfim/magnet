import React from 'react';

import './styles.css';
import data from './data.json';
import estadosCidades from './estados-cidades.json';
import PropType from 'prop-types';
import NumberFormat from 'react-number-format';

export const Input = ({ container, name, placeholder, required, label, value, type, update, format }) => {
  return (
    <div className={`${container}__input`}>
      <div className="label-wrapper">
        {label && (
          <label className="label" htmlFor={`${container}_${name}`}>
            {label}
          </label>
        )}
        <p className={`${container}_${name} label-error`}>- Preencha esse campo</p>
      </div>
      {!format ? (
        <input
          className="input"
          type={type}
          name={`${container}_${name}`}
          id={`${container}_${name}`}
          placeholder={placeholder}
          value={value}
          onChange={(e) => update(name, e.target.value)}
          required={required ? true : false}
        />
      ) : (
        <NumberFormat
          className="input"
          format="(##) #####-####"
          name={`${container}_${name}`}
          id={`${container}_${name}`}
          placeholder={placeholder}
          value={value}
          onChange={(e) => update(name, e.target.value)}
          required={required ? true : false}
        />
      )}
    </div>
  );
};

export const Textarea = ({ container, name, placeholder, required, label, value, update }) => {
  return (
    <div className={`${container}__input`}>
      <div className="label-wrapper">
        {label && (
          <label className="label" htmlFor={`${container}_${name}`}>
            {label}
          </label>
        )}
        <p className={`${container}_${name} label-error`}>- Digite algo</p>
      </div>
      <textarea
        className="textarea"
        name={`${container}_${name}`}
        id={`${container}_${name}`}
        placeholder={placeholder}
        value={value}
        onChange={(e) => update(name, e.target.value)}
        required={required ? true : false}
      />
    </div>
  );
};

export const Select = ({ container, name, required, label, value, update, typeData, estado }) => {
  if (typeData === 'estado') {
    return (
      <div className={`${container}__input`}>
        <div className="label-wrapper">
          {label && (
            <label className="label" htmlFor={`${container}_${name}`}>
              {label}
            </label>
          )}
          <p className={`${container}_${name} label-error`}>- Digite algo</p>
        </div>
        <select
          className="input"
          name={`${container}_${name}`}
          id={`${container}_${name}`}
          value={value}
          onChange={(e) => update(name, e.target.value)}
          required={required ? true : false}
        >
          <option value="">Selecione</option>
          {estadosCidades['estados'].map((el, index) => {
            return (
              <option value={el.nome} key={el.nome}>
                {el.nome}
              </option>
            );
          })}
        </select>
      </div>
    );
  } else if (typeData === 'cidade') {
    const cidades = estadosCidades['estados'].filter((el) => el.nome === estado)[0]?.cidades || [];
    return (
      <div className={`${container}__input`}>
        <div className="label-wrapper">
          {label && (
            <label className="label" htmlFor={`${container}_${name}`}>
              {label}
            </label>
          )}
          <p className={`${container}_${name} label-error`}>- Digite algo</p>
        </div>
        <select
          className="input"
          name={`${container}_${name}`}
          id={`${container}_${name}`}
          value={value}
          onChange={(e) => update(name, e.target.value)}
          required={required ? true : false}
        >
          <option value="">Selecione</option>
          {cidades !== [] &&
            cidades.map((el, index) => {
              return (
                <option value={el} key={el}>
                  {el}
                </option>
              );
            })}
        </select>
      </div>
    );
  } else {
    return (
      <div className={`${container}__input`}>
        <div className="label-wrapper">
          {label && (
            <label className="label" htmlFor={`${container}_${name}`}>
              {label}
            </label>
          )}
          <p className={`${container}_${name} label-error`}>- Digite algo</p>
        </div>
        <select
          className="input"
          name={`${container}_${name}`}
          id={`${container}_${name}`}
          value={value}
          onChange={(e) => update(name, e.target.value)}
          required={required ? true : false}
        >
          <option value="">Selecione</option>
          {data[typeData].map((el, index) => {
            return (
              <option value={el} key={el}>
                {el}
              </option>
            );
          })}
        </select>
      </div>
    );
  }
};

Input.propTypes = {
  container: PropType.string.isRequired,
  name: PropType.string.isRequired,
  placeholder: PropType.string.isRequired,
  required: PropType.bool,
  label: PropType.string.isRequired,
  value: PropType.string.isRequired,
  type: PropType.string.isRequired,
  update: PropType.func.isRequired,
  format: PropType.string,
};

Textarea.propTypes = {
  container: PropType.string.isRequired,
  name: PropType.string.isRequired,
  placeholder: PropType.string.isRequired,
  required: PropType.bool,
  label: PropType.string.isRequired,
  value: PropType.string.isRequired,

  update: PropType.func.isRequired,
};

Select.propTypes = {
  container: PropType.string.isRequired,
  name: PropType.string.isRequired,
  required: PropType.bool,
  label: PropType.string.isRequired,
  value: PropType.string.isRequired,

  update: PropType.func.isRequired,
  typeData: PropType.string.isRequired,
  estado: PropType.string,
};

Select.defaultProps = {
  estado: '',
};
