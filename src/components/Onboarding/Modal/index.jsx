import React from 'react';
import PropTypes from 'prop-types';

import { loadFromLocalstorage, removeLocalStorage } from 'utils/localStorage';

import './style.css';
import { Link, useHistory } from 'react-router-dom';
import { OnboardingBtn } from 'components/Buttons/Onboarding';

export const OnboardingModal = ({ setRegister, setDataSaved, setStep, dataSaved: data }) => {
  const { register, step } = loadFromLocalstorage('onboarding');
  const history = useHistory();

  const handleContinue = () => {
    setRegister({
      ...register,
      password: '',
      confirmPassword: '',
    });
    setStep(step);
    history.push('/onboarding/1');
    setDataSaved({ open: false, type: '' });
  };

  const handleConflit = () => {
    setDataSaved({ open: false, type: '' });
  };

  const handleCancel = () => {
    removeLocalStorage('onboarding');
    setDataSaved({ open: false, type: '' });
  };

  if (data.type === 'data') {
    return (
      <div className="onboarding-modal">
        <div className="onboarding-modal__content">
          <p className="onboarding-modal__title">
            Olá, {register.name}! Vimos que você já começou um cadastro. Deseja continuar?
          </p>
          <OnboardingBtn text="Continuar" handleNext={handleContinue} />
          <Link to="/onboarding/0" onClick={handleCancel} className="onboarding-modal__button-cancel">
            Cancelar
          </Link>
        </div>
      </div>
    );
  } else {
    return (
      <div className="onboarding-modal">
        <div className="onboarding-modal__content">
          <p className="onboarding-modal__title">
            Olá! Verificamos que já existe um cadastro vinculado a
            {data.type === 'company' ? ' essa empresa' : ' esse e-mail'}. Deseja alterar?
          </p>
          <OnboardingBtn text="Alterar Cadastro" handleNext={handleConflit} />
          <Link to="/onboarding/0" onClick={handleCancel} className="onboarding-modal__button-cancel">
            Cancelar
          </Link>
        </div>
      </div>
    );
  }
};

OnboardingModal.propTypes = {
  setDataSaved: PropTypes.func.isRequired,
  setRegister: PropTypes.func.isRequired,
  setStep: PropTypes.func.isRequired,
  dataSaved: PropTypes.object.isRequired,
};
