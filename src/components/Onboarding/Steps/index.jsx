import React from 'react';
import PropTypes from 'prop-types';

import './styles.css';
import { Input, Select } from '../../Inputs';
import { OnboardingBtn } from '../../Buttons/Onboarding';
import { Link } from 'react-router-dom';

export const FirstPage = ({ handleNext }) => {
  return (
    <div className="onboarding__user">
      <div className="onboarding__logo">
        <img src="/images/logos/logo.png" alt="Magnet Logo" />
      </div>
      <h3 className="onboarding__question" style={{ marginBottom: 50 }}>
        Você está criando uma conta no Módulo Comercial da Magnet Customer.
      </h3>
      <p className="onboarding__description" style={{ marginBottom: 10 }}>
        Durante os próximos 21 dias, você vai utilizar gratuitamente a nossa ferramenta, após esse período você pode
        ativar a assinatura ou desativar a sua conta.
      </p>

      <OnboardingBtn text="Continuar" handleNext={handleNext} />
      <Link to="/contato" className="firstpage__link">
        Caso deseje falar direto com nosso consultor, basta clicar aqui.
      </Link>
    </div>
  );
};

export const User = ({ step, register: { name, lastname, email, phone }, updateOne, handleNext }) => {
  const newStep = Number(step);

  return (
    <React.Fragment>
      {newStep === 1 && (
        <div className="onboarding__user">
          <h3 className="onboarding__question">Olá! Qual o seu nome e sobrenome?</h3>
          <p className="onboarding__description">
            Os dados solicitados serão utilizados para construirmos sua plataforma e cadastro
          </p>

          <div className="onboarding__half-inputs">
            <Input
              type="text"
              placeholder="Insira seu nome"
              label="Nome"
              container="user"
              name="name"
              value={name}
              update={updateOne}
            />
            <Input
              type="text"
              placeholder="Insira seu sobrenome"
              label="Sobrenome"
              container="user"
              name="lastname"
              value={lastname}
              update={updateOne}
            />
          </div>
          <OnboardingBtn text="Próximo" handleNext={handleNext} />
        </div>
      )}
      {newStep === 2 && (
        <div className="onboarding__user">
          <h3 className="onboarding__question">{name}, informe seus dados de contato.</h3>
          <p className="onboarding__description">Prometemos não encher sua caixa de spam!</p>
          <div className="onboarding__half-inputs">
            <Input
              type="text"
              placeholder="Insira seu melhor e-mail"
              label="E-mail"
              container="user"
              name="email"
              value={email}
              update={updateOne}
            />

            <Input
              type="phone"
              placeholder="(00) 00000-0000"
              format="(##) #####-####"
              label="Telefone"
              class="input"
              container="user"
              name="phone"
              value={phone}
              update={updateOne}
            />
          </div>
          <OnboardingBtn text="Próximo" handleNext={handleNext} />
        </div>
      )}
    </React.Fragment>
  );
};

export const Company = ({
  step,
  updateOne,
  updateTwo,
  handlePassword,
  handleNext,
  register: { name, company, password, confirmPassword, role },
}) => {
  const newStep = Number(step);
  return (
    <React.Fragment>
      {newStep === 3 && (
        <div className="onboarding__user">
          <h3 className="onboarding__question">Vamos conhecer a sua empresa</h3>
          <p className="onboarding__description">
            Assim como as informações anteriores, utilizaremos essas para criarmos um atendimento ainda mais
            personalizado para você
          </p>
          <div className="onboarding__half-inputs">
            <Input
              type="text"
              placeholder="Insira o nome fantasia"
              label="Nome Fantasia"
              container="company"
              name="name"
              value={company.name}
              update={updateTwo}
            />
            <Select
              label="Segmento da empresa"
              container="company"
              name="segment"
              value={company.segment}
              update={updateTwo}
              typeData="segment"
            />
          </div>
          <div className="onboarding__half-inputs">
            <Select
              label="Quantidade de funcionários"
              container="company"
              name="size"
              value={company.size}
              update={updateTwo}
              typeData="size"
            />

            <Input
              type="text"
              placeholder="Insira seu cargo na empresa"
              label="Seu cargo"
              container="role"
              name="role"
              value={role}
              update={updateOne}
            />
          </div>
          <OnboardingBtn text="Próximo" handleNext={handleNext} />
        </div>
      )}
      {newStep === 4 && (
        <div className="onboarding__user">
          <h3 className="onboarding__question">{name}, crie uma senha.</h3>
          <p className="onboarding__description">
            Sua senha deve conter no minimo 8 caracteres, com pelo menos uma letra maiúscula, uma minúscula, um número e
            um caractere especial.
          </p>
          <div className="onboarding__half-inputs">
            <Input
              type="password"
              placeholder="Insira uma senha"
              label="Senha"
              container="user"
              name="password"
              value={password}
              update={updateOne}
            />
            <Input
              type="password"
              placeholder="Confirme sua senha"
              label="Confirmação de senha"
              container="user"
              name="confirmPassword"
              value={confirmPassword}
              update={updateOne}
            />
          </div>
          <OnboardingBtn text="Próximo" handleNext={handleNext} handlePassword={handlePassword} />
        </div>
      )}
    </React.Fragment>
  );
};
export const Checked = ({ handleNext }) => {
  return (
    <div className="onboarding__user">
      <h3 className="onboarding__question">Parabéns! Você acaba de fazer a diferença para o seu negócio.</h3>
      <p className="onboarding__description">
        Em breve, você receberá o contato do nosso analista para uma primeira demonstração do uso da plataforma.
      </p>

      <OnboardingBtn text="Acessar Módulo Comercial" handleNext={handleNext} />
    </div>
  );
};

FirstPage.propTypes = {
  handleNext: PropTypes.func.isRequired,
};

User.propTypes = {
  step: PropTypes.number.isRequired,
  register: PropTypes.shape({
    name: PropTypes.string.isRequired,
    lastname: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired,
  }),
  updateOne: PropTypes.func.isRequired,
  handleNext: PropTypes.func.isRequired,
};

Company.propTypes = {
  step: PropTypes.number.isRequired,
  register: PropTypes.shape({
    name: PropTypes.string.isRequired,
    role: PropTypes.string.isRequired,
    company: PropTypes.object.isRequired,
    password: PropTypes.string,
    confirmPassword: PropTypes.string,
  }),
  updateOne: PropTypes.func.isRequired,
  updateTwo: PropTypes.func.isRequired,
  handlePassword: PropTypes.func.isRequired,
  handleNext: PropTypes.func.isRequired,
};

Checked.propTypes = {
  handleNext: PropTypes.func.isRequired,
};
