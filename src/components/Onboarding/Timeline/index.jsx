import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

import './styles.css';

export const Timeline = ({ step }) => {
  const one = useRef();
  const two = useRef();
  const three = useRef();

  useEffect(() => {
    if (step <= 2) {
      one.current.classList.add('onboarding__timeline-ball--in');
      two.current.classList.remove('onboarding__timeline-ball--in');
      three.current.classList.remove('onboarding__timeline-ball--in');
    } else if (step <= 4) {
      one.current.classList.add('onboarding__timeline-ball--in');
      two.current.classList.add('onboarding__timeline-ball--in');
      three.current.classList.remove('onboarding__timeline-ball--in');
    } else if (step > 4) {
      one.current.classList.add('onboarding__timeline-ball--in');
      two.current.classList.add('onboarding__timeline-ball--in');
      three.current.classList.add('onboarding__timeline-ball--in');
    }
  }, [step]);

  return (
    <div className="onboarding__timeline">
      <div className="onboarding__timeline-ball " ref={one}>
        <svg xmlns="http://www.w3.org/2000/svg" width="32.222" height="38.621" viewBox="0 0 32.222 38.621">
          <g transform="translate(0 0)">
            <path
              d="M96.334,18.6a9,9,0,0,0,6.577-2.725A9,9,0,0,0,105.636,9.3a9,9,0,0,0-2.725-6.577,9.3,9.3,0,0,0-13.154,0A9,9,0,0,0,87.032,9.3a9,9,0,0,0,2.725,6.577A9,9,0,0,0,96.334,18.6ZM91.358,4.325a7.036,7.036,0,0,1,9.953,0A6.725,6.725,0,0,1,103.373,9.3a6.725,6.725,0,0,1-2.062,4.977,7.036,7.036,0,0,1-9.953,0A6.724,6.724,0,0,1,89.295,9.3a6.724,6.724,0,0,1,2.063-4.976Zm0,0"
              transform="translate(-80.467 0)"
            />
            <path
              d="M32.144,258.359a22.977,22.977,0,0,0-.313-2.44,19.227,19.227,0,0,0-.6-2.454,12.12,12.12,0,0,0-1.009-2.288,8.626,8.626,0,0,0-1.521-1.982,6.706,6.706,0,0,0-2.185-1.373,7.551,7.551,0,0,0-2.789-.5,2.831,2.831,0,0,0-1.512.641c-.453.3-.983.637-1.575,1.015a9.024,9.024,0,0,1-2.038.9,7.913,7.913,0,0,1-4.985,0,9,9,0,0,1-2.036-.9c-.586-.375-1.116-.716-1.576-1.016a2.828,2.828,0,0,0-1.511-.641,7.541,7.541,0,0,0-2.789.505,6.7,6.7,0,0,0-2.185,1.373A8.628,8.628,0,0,0,2,251.176,12.142,12.142,0,0,0,.99,253.465a19.272,19.272,0,0,0-.6,2.453,22.821,22.821,0,0,0-.313,2.441C.026,259.1,0,259.865,0,260.64a6.413,6.413,0,0,0,1.9,4.852,6.856,6.856,0,0,0,4.908,1.79h18.6a6.857,6.857,0,0,0,4.908-1.79,6.411,6.411,0,0,0,1.9-4.852c0-.778-.027-1.546-.078-2.281Zm-3.387,5.494a4.616,4.616,0,0,1-3.348,1.167H6.813a4.616,4.616,0,0,1-3.348-1.166,4.188,4.188,0,0,1-1.2-3.212c0-.724.024-1.438.072-2.124a20.583,20.583,0,0,1,.283-2.2,17,17,0,0,1,.528-2.163,9.886,9.886,0,0,1,.821-1.861,6.4,6.4,0,0,1,1.118-1.465,4.449,4.449,0,0,1,1.454-.9,5.208,5.208,0,0,1,1.782-.344c.079.042.22.123.449.272.465.3,1,.649,1.594,1.028a11.214,11.214,0,0,0,2.558,1.143,10.175,10.175,0,0,0,6.377,0,11.226,11.226,0,0,0,2.559-1.144c.607-.388,1.128-.724,1.593-1.027.229-.149.37-.23.449-.272a5.211,5.211,0,0,1,1.783.344,4.455,4.455,0,0,1,1.453.9,6.378,6.378,0,0,1,1.118,1.465,9.852,9.852,0,0,1,.821,1.86,16.959,16.959,0,0,1,.528,2.163,20.758,20.758,0,0,1,.283,2.2h0c.048.683.072,1.4.072,2.123a4.187,4.187,0,0,1-1.2,3.212Zm0,0"
              transform="translate(0 -228.661)"
            />
          </g>
        </svg>
      </div>
      <div className="onboarding__timeline-ball " ref={two}>
        <svg xmlns="http://www.w3.org/2000/svg" width="41.667" height="41.667" viewBox="0 0 41.667 41.667">
          <g transform="translate(0 0)">
            <g transform="translate(14.606 13.672)">
              <g transform="translate(0 0)">
                <rect width="1.217" height="1.826" />
              </g>
            </g>
            <g transform="translate(14.606 16.715)">
              <g transform="translate(0 0)">
                <rect width="1.217" height="1.217" />
              </g>
            </g>
            <g transform="translate(14.606 19.149)">
              <g transform="translate(0 0)">
                <rect width="1.217" height="1.826" />
              </g>
            </g>
            <g transform="translate(14.606 22.192)">
              <g transform="translate(0 0)">
                <rect width="1.217" height="1.217" />
              </g>
            </g>
            <g transform="translate(17.649 13.672)">
              <g transform="translate(0 0)">
                <rect width="1.217" height="1.826" />
              </g>
            </g>
            <g transform="translate(17.649 16.715)">
              <g transform="translate(0 0)">
                <rect width="1.217" height="1.217" />
              </g>
            </g>
            <g transform="translate(17.649 19.149)">
              <g transform="translate(0 0)">
                <rect width="1.217" height="1.826" />
              </g>
            </g>
            <g transform="translate(17.649 22.192)">
              <g transform="translate(0 0)">
                <rect width="1.217" height="1.217" />
              </g>
            </g>
            <g transform="translate(20.083 13.672)">
              <g transform="translate(0 0)">
                <rect width="1.217" height="1.826" />
              </g>
            </g>
            <g transform="translate(20.083 16.715)">
              <g transform="translate(0 0)">
                <rect width="1.217" height="1.217" />
              </g>
            </g>
            <g transform="translate(20.083 19.149)">
              <g transform="translate(0 0)">
                <rect width="1.217" height="1.826" />
              </g>
            </g>
            <g transform="translate(20.083 22.192)">
              <g transform="translate(0 0)">
                <rect width="1.217" height="1.217" />
              </g>
            </g>
            <g transform="translate(23.127 13.672)">
              <g transform="translate(0 0)">
                <rect width="1.217" height="1.826" />
              </g>
            </g>
            <g transform="translate(17.649 11.237)">
              <g transform="translate(0 0)">
                <rect width="1.217" height="1.217" />
              </g>
            </g>
            <g transform="translate(20.083 11.237)">
              <g transform="translate(0 0)">
                <rect width="1.217" height="1.217" />
              </g>
            </g>
            <g transform="translate(23.127 11.237)">
              <g transform="translate(0 0)">
                <rect width="1.217" height="1.217" />
              </g>
            </g>
            <g transform="translate(23.127 16.715)">
              <g transform="translate(0 0)">
                <rect width="1.217" height="1.217" />
              </g>
            </g>
            <g transform="translate(23.127 19.149)">
              <g transform="translate(0 0)">
                <rect width="1.217" height="1.826" />
              </g>
            </g>
            <g transform="translate(23.127 22.192)">
              <g transform="translate(0 0)">
                <rect width="1.217" height="1.217" />
              </g>
            </g>
            <g transform="translate(25.561 13.672)">
              <g transform="translate(0 0)">
                <rect width="1.826" height="1.826" />
              </g>
            </g>
            <g transform="translate(25.561 16.715)">
              <g transform="translate(0 0)">
                <rect width="1.826" height="1.217" />
              </g>
            </g>
            <g transform="translate(25.561 19.149)">
              <g transform="translate(0 0)">
                <rect width="1.826" height="1.826" />
              </g>
            </g>
            <g transform="translate(25.561 22.192)">
              <g transform="translate(0 0)">
                <rect width="1.826" height="1.217" />
              </g>
            </g>
            <g transform="translate(0 0)">
              <g transform="translate(0 0)">
                <path
                  d="M40.973,10.417H30.556V.694A.694.694,0,0,0,29.862,0H11.806a.694.694,0,0,0-.694.694h0v9.722H.694A.694.694,0,0,0,0,11.11H0v25a.694.694,0,0,0,.694.694H2.146a4.167,4.167,0,0,0-.063.694v3.472a.694.694,0,0,0,.694.694H38.889a.694.694,0,0,0,.694-.694h0V37.5a4.149,4.149,0,0,0-.063-.694h1.452a.694.694,0,0,0,.694-.694h0v-25A.694.694,0,0,0,40.973,10.417ZM29.861,29.861a1.389,1.389,0,1,1-1.389-1.389A1.389,1.389,0,0,1,29.861,29.861ZM13.195,28.472a1.389,1.389,0,1,1-1.389,1.389A1.389,1.389,0,0,1,13.195,28.472ZM9.028,40.278H3.472V37.5a2.778,2.778,0,1,1,5.556,0ZM4.861,31.945A1.389,1.389,0,1,1,6.25,33.334,1.389,1.389,0,0,1,4.861,31.945ZM9.133,34.5a4.186,4.186,0,0,0-.874-.647,2.778,2.778,0,1,0-4.019,0,4.194,4.194,0,0,0-1.594,1.565H1.389V11.806h9.722V28.043a2.74,2.74,0,0,0,.074,3.726A4.178,4.178,0,0,0,9.133,34.5Zm6.839,5.779H10.417V35.417a2.778,2.778,0,0,1,5.556,0Zm8.333,0H17.361V35.417a3.472,3.472,0,0,1,6.945,0ZM18.75,28.472a2.083,2.083,0,1,1,2.083,2.083A2.083,2.083,0,0,1,18.75,28.472Zm6.313,4.557a4.891,4.891,0,0,0-1.992-1.924,3.472,3.472,0,1,0-4.474,0A4.892,4.892,0,0,0,16.6,33.029a4.194,4.194,0,0,0-1.4-1.26,2.758,2.758,0,0,0-2.7-4.587V1.389H29.167V27.182a2.766,2.766,0,0,0-2.7,4.587A4.194,4.194,0,0,0,25.063,33.029ZM31.25,37.5v2.778H25.695V35.417a2.778,2.778,0,0,1,5.556,0Zm6.944,2.778H32.639V37.5a2.778,2.778,0,1,1,5.556,0Zm-4.167-8.333a1.389,1.389,0,1,1,1.389,1.389A1.389,1.389,0,0,1,34.028,31.945Zm6.25,3.472H39.02a4.194,4.194,0,0,0-1.594-1.565,2.778,2.778,0,1,0-4.019,0,4.184,4.184,0,0,0-.874.647,4.178,4.178,0,0,0-2.052-2.73,2.74,2.74,0,0,0,.074-3.726V11.806h9.722V35.417Z"
                  transform="translate(0 0)"
                />
              </g>
            </g>
            <g transform="translate(32.864 16.106)">
              <g transform="translate(0 0)">
                <rect width="1.217" height="10.955" />
              </g>
            </g>
            <g transform="translate(36.516 16.106)">
              <g transform="translate(0 0)">
                <rect width="1.826" height="10.955" />
              </g>
            </g>
            <g transform="translate(3.651 16.106)">
              <g transform="translate(0 0)">
                <rect width="1.217" height="10.955" />
              </g>
            </g>
            <g transform="translate(7.912 16.106)">
              <g transform="translate(0 0)">
                <rect width="1.217" height="10.955" />
              </g>
            </g>
            <g transform="translate(15.972 2.778)">
              <path
                d="M193.029,32h-8.334a.694.694,0,0,0-.694.694h0V38.25a.694.694,0,0,0,.694.694h8.334a.694.694,0,0,0,.694-.694h0V32.694A.694.694,0,0,0,193.029,32Zm-.7,5.556h-6.945V33.389h6.945Z"
                transform="translate(-184 -32)"
              />
            </g>
          </g>
        </svg>
      </div>
      <div className="onboarding__timeline-ball " ref={three}>
        <svg xmlns="http://www.w3.org/2000/svg" width="37.651" height="37.651" viewBox="0 0 37.651 37.651">
          <g transform="translate(0)">
            <g transform="translate(8.94 4.73)">
              <path
                d="M149.2,64.744a1.419,1.419,0,0,0-2.006,0L130.173,81.72l-6.142-6.671a1.419,1.419,0,0,0-2.088,1.922l7.142,7.757a1.417,1.417,0,0,0,1.014.457h.03a1.42,1.42,0,0,0,1-.414L149.2,66.751A1.419,1.419,0,0,0,149.2,64.744Z"
                transform="translate(-121.568 -64.327)"
              />
            </g>
            <g transform="translate(0)">
              <path d="M36.232,17.407a1.419,1.419,0,0,0-1.419,1.419A15.988,15.988,0,1,1,18.826,2.838a1.419,1.419,0,1,0,0-2.838A18.826,18.826,0,1,0,37.651,18.826,1.419,1.419,0,0,0,36.232,17.407Z" />
            </g>
          </g>
        </svg>
      </div>
    </div>
  );
};

Timeline.propTypes = {
  step: PropTypes.number.isRequired,
};
