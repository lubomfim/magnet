import React from 'react';
import './styles.css';

export const Partners = () => {
  return (
    <section className="partners">
      <div className="partners__wrapper">
        <h2 className="partners__title">Alguns dos nossos clientes</h2>
        <p className="partners__description">Mais do que clientes, parceiros!</p>

        <ul className="partners__list">
          <li className="partners__item">
            <img src="/images/partners/anhanguera.png" alt="Partner" className="partners__item-img" />
          </li>
          <li className="partners__item">
            <img src="/images/partners/bossanova.png" alt="Partner" className="partners__item-img" />
          </li>
          <li className="partners__item">
            <img src="/images/partners/caoa-cherry.png" alt="Partner" className="partners__item-img" />
          </li>
          <li className="partners__item">
            <img src="/images/partners/estacio.png" alt="Partner" className="partners__item-img" />
          </li>
          <li className="partners__item">
            <img src="/images/partners/caoa.png" alt="Partner" className="partners__item-img" />
          </li>
          <li className="partners__item">
            <img src="/images/partners/porsche.png" alt="Partner" className="partners__item-img" />
          </li>
        </ul>
      </div>
    </section>
  );
};
