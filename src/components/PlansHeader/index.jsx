import { CTA } from 'components/Buttons/CTA';
import React from 'react';

import './styles.css';

const plansData = [
  {
    title: 'Módulo Comercial',
    className: 'modulo-comecial__card',
    about:
      'Ideal para empresas que precisam ter seus processos comerciais organizados e gerando resultados financeiros.',
    ctaText: 'Comece agora',
    ctaColor: '#E3E3E3 ',
    features: [
      'controle o fluxo de vendas',
      'cadastro de pessoas e empresas',
      'cadastro de produtos e serviços',
      'calendário de atividades',
      'gere a sua proposta em PDF',
      'histórico de atendimento',
    ],
    price: '49,90',
    priceObs: 'BRL/usuário/mês**',
    ctaBottom: false,
  },
  {
    title: 'CRM Completo',
    className: 'crm-completo__card',
    about: 'Ideal para empresas que precisam construir relacionamento com os clientes e gerar mais negócios.',
    ctaText: 'Fale com especialista',
    features: [
      'atendimento multicanais',
      'geração de campanhas',
      'histórico do cliente',
      'análise de oportunidade',
      'análise de engajamento',
      'dashboard customizável',
      'business inteligence',
      'data-driven',
      'integrações',
    ],
    ctaBottom: true,
  },
];

export const PlansHeader = () => {
  return (
    <div className="plansHeader">
      <div className="plansHeader__wrapper">
        <div className="plansHeader__text">
          <h2 className="plansHeader__title">
            Planos <span className="plansHeader__span">Magnet Customer</span>
          </h2>
          <p className="plansHeader__description">
            O marketing de relacionamento integrado ao atendimento lhe permite ter uma visão 360º do seu cliente.
          </p>
        </div>
        <div className="plansHeader__cards">
          {plansData.map((el, index) => {
            return (
              <div className={`plansHeader__card ${el.className}`} key={index}>
                <p className="plansHeader__card-title">{el.title}</p>
                <p className="plansHeader__card-description">{el.about}</p>
                <CTA text={el.ctaText} type={el.title === 'CRM Completo' ? 'especialista' : ''} />
                <ul className="plansHeader__card-list">
                  <li className="plansHeader__card-list-title">Funcionalidades:</li>
                  {el.features.map((elFeature, indexFeature) => {
                    return (
                      <li className="plansHeader__card-list-item" key={indexFeature}>
                        <img src="/images/icons/check.svg" alt="Check Icon" />
                        <p>{elFeature}</p>
                      </li>
                    );
                  })}
                </ul>
                {el.ctaBottom && <p className="plansHeader__additional">+ Módulo Comercial</p>}
                {el.price && (
                  <p className="plansHeader__card-price">
                    R$ <span>{el.price}</span>
                  </p>
                )}
                {el.priceObs && <small>{el.priceObs}</small>}
                {el.ctaBottom && <CTA text={el.ctaText} />}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};
