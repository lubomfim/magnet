import React from 'react';
import { Link } from 'react-router-dom';

import './style.css';

const DataSent = () => {
  return (
    <div className="partner-sent">
      <div className="partner__wrapper">
        <div className="partner__text">
          <h2 className="partner__title">Formulário enviado!</h2>
          <p className="partner__description">
            O formulário foi enviado, e o contato do nosso time de especialistas deve ser feito em breve!
          </p>
        </div>
        <Link to="/home" className="partner__come-back">
          Voltar para página principal
        </Link>
      </div>
    </div>
  );
};

export default DataSent;
