import Cookies from 'components/Cookies';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <App />
    <Cookies />
  </React.StrictMode>,
  document.getElementById('root'),
);
