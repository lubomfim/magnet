import React from 'react';
import './About.css';

import { Header } from 'components/Header';
import { Footer } from 'components/Footer';

const About = (props) => {
  return (
    <React.Fragment>
      <Header />
      <div className="about">
        <div className="about-hero">
          <div className="about-hero__img">
            <img src="/images/logos/icone.png" alt="Icone Magnet" />
          </div>
        </div>
        <div className="container">
          <div className="about__wrapper">
            <section className="about__section">
              <h2 className="about__subtitle">O que é a Magnet Customer?</h2>
              <p className="about__paragraph">
                A Magnet Customer é uma empresa de tecnologia que nasceu para ajudar empresas a construir relacionamento
                com seus clientes e gerar mais negócios. Fazemos isso através de software, consultoria, terceirização e
                treinamento.
              </p>
              <p className="about__paragraph">A Magnet Customer é uma solução completa de CRM para que sua empresa.</p>
            </section>
            <section className="about__section">
              <h2 className="about__subtitle">O que fazemos</h2>
              <p className="about__paragraph">
                Ajudamos empresas, de qualquer tamanho ou segmento, a implementar ou aprimorar a cultura de CRM.
              </p>
              <p className="about__paragraph">
                Fazemos isso por meio da nossa Plataforma de CRM – Magnet Customer ou por meio dos serviços de
                consultoria, terceirização ou treinamentos. Desta forma conseguimos entregar, para nossos clientes, uma
                solução completa e personalizada de construção de relacionamento com clientes. Vendas - Pós-Vendas –
                Atendimento – Fidelização
              </p>
            </section>
            <section className="about__section">
              <h2 className="about__subtitle">Como fazemos</h2>
              <p className="about__paragraph">
                Unimos o Marketing de relacionamento, o atendimento ao cliente e suas informações em um único lugar.
                Automatizamos os processos de comunicação, permitindo definir, com exatidão, o que comunicar, pra quem e
                quando. Tudo isso integrado aos principais canais de comunicação. Agora qualquer equipe de
                relacionamento consegue acompanhar toda evolução da jornada de dos clientes de maneira simples e
                confiável, tudo está em um único lugar.
              </p>
              <p className="about__paragraph">
                Conseguimos fazer isso graças ao nosso algoritmo de inteligência que estrutura os dados (histórico de
                compra, atendimento e outros) das empresas e entende os padrões de comportamento, traduzindo-os em
                insights para as ações automatizadas.
              </p>
            </section>
            <section className="about__section">
              <h2 className="about__subtitle">Nossa filosofia</h2>
              <p className="about__paragraph">
                Acreditamos que o sucesso de qualquer empresa está na maneira que ela se relaciona com seus clientes.
              </p>
              <p className="about__paragraph">
                Aqui na Magnet Customer, procuramos entregar para nossos clientes o nosso melhor. Sempre respeitando as
                peculiaridades e momento de cada um. Nosso mantra é: <i>O que você faria se fosse com você?</i>
              </p>
            </section>
          </div>
        </div>
      </div>
      <Footer />
    </React.Fragment>
  );
};

export default About;
