import React from 'react';
import './Blog.css';

import { Header } from 'components/Header';
import { Footer } from 'components/Footer';
import { BlogHeader } from 'components/Hero/Blog/';
import { Link } from 'react-router-dom';
import { CTA } from 'components/Buttons/CTA/';

const sectionData = {
  title: 'O que é CRM?',
  description:
    'CRM, sigla para Customer Relationship Management, é uma ferramenta de vendas para registrar e organizar todos pontos de um contato que um consumidor tem com o vendedor de uma empresa.',
  illustration: 'painel',
  cta: 'Experimente nosso CRM',
};

const Blog = (props) => {
  const { title, description, illustration, cta } = sectionData;
  return (
    <div className="blog">
      <Header />
      <BlogHeader title={title} description={description} illustration={illustration} cta={cta} />
      <main className="blog__wrapper">
        <div className="blog__text">
          <h2 className="blog__title">Como o CRM pode te ajudar a crescer?</h2>
          <p className="blog__description">
            CRM, sigla para Customer Relationship Management, é uma ferramenta de vendas para registrar e organizar
            todos pontos.
          </p>
        </div>
        <ul className="blog__list">
          <li className="blog__item">
            <div className="blog__item-text">
              <h3 className="blog__item-title">Gerenciamento do seu usuário</h3>
              <p className="blog__item-description">
                Customer Relationship Management é um termo em inglês que pode ser traduzido.
              </p>
              <Link to="/onboarding/0" className="blog__veja-mais">
                Veja mais
              </Link>
            </div>
            <div className="blog__item-illustration">
              <img src="/images/blog/first-image.png" alt="Dashboard illustration" />
            </div>
          </li>
          <li className="blog__item">
            <div className="blog__item-illustration">
              <img src="/images/blog/second-image.png" alt="Dashboard illustration" />
            </div>
            <div className="blog__item-text">
              <h3 className="blog__item-title">Gerenciamento do seu usuário</h3>
              <p className="blog__item-description">
                Customer Relationship Management é um termo em inglês que pode ser traduzido.
              </p>
              <Link to="/onboarding/0" className="blog__veja-mais">
                Veja mais
              </Link>
            </div>
          </li>
        </ul>
      </main>
      <section className="blog__dashboard">
        <div className="blog__dashboard-img">
          <img src="/images/crm-print.png" alt="Magnet Dashboard" />
          <CTA text="Experimente nosso CRM" />
        </div>
      </section>
      <Footer />
    </div>
  );
};

export default Blog;
