import React, { useEffect, useState } from 'react';
import './Contact.css';

import { Header } from 'components/Header';
import { Footer } from 'components/Footer';
import { Partners } from 'components/Partners/';
import DataSent from '../../components/sent';

import { Input, Textarea } from 'components/Inputs/';
import { sendContact } from 'utils/sendEmail';

const Contact = (props) => {
  const [form, setForm] = useState({
    nome: '',
    email: '',
    empresa: '',
    mensagem: '',
  });
  const [sent, setSent] = useState(false);

  const updateState = (type, value) => {
    setForm({
      ...form,
      [type]: value,
    });
  };

  useEffect(() => {
    return () => {
      setSent(false);
      setForm({
        nome: '',
        email: '',
        empresa: '',
        mensagem: '',
      });
    };
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await sendContact(form);
      setSent(true);
    } catch (err) {
      alert(err);
      setSent(false);
    }
  };

  return (
    <React.Fragment>
      {sent && <DataSent />}
      {!sent && (
        <div>
          <Header />
          <div className="contact">
            <div className="contact__wrapper">
              <div className="container">
                <div className="contact__hero">
                  <h2 className="contact__hero-title">Solicite uma demonstração ou contato</h2>
                  <div className="contact__hero-illustration">
                    <img src="/images/contact/illustration.svg" alt="Phone Illustration" />
                  </div>
                </div>
              </div>
            </div>
            <div className="container">
              <div className="contact__content">
                <form className="contact__form" onSubmit={handleSubmit}>
                  <Input
                    label="Nome"
                    container="contact"
                    type="text"
                    name="nome"
                    placeholder="Insira seu nome"
                    update={updateState}
                    value={form.name}
                    required={true}
                  />
                  <Input
                    label="E-mail"
                    container="contact"
                    type="email"
                    name="email"
                    placeholder="Insira seu melhor e-mail"
                    update={updateState}
                    value={form.email}
                    required={true}
                  />
                  <Input
                    label="Empresa"
                    container="contact"
                    type="empresa"
                    name="empresa"
                    placeholder="Insira o nome da sua Empresa"
                    update={updateState}
                    value={form.empresa}
                    required={true}
                  />
                  <Textarea
                    label="Como podemos te ajudar?"
                    container="contact"
                    name="mensagem"
                    placeholder="Insira o nome da sua Mensagem"
                    update={updateState}
                    value={form.mensagem}
                  />
                  <button className="cta__button" type="submit">
                    Enviar
                  </button>
                </form>
                <div className="contact__text">
                  <div className="contact__text-title-wrapper">
                    <h2 className="contact__text-title">Após o envio, nosso time entrará em contato com você!</h2>
                  </div>
                  <ul className="contact__text-list">
                    <li className="contact__text-item">- Totalmente Gratuito</li>
                    <li className="contact__text-item">- Demonstração ao vivo</li>
                    <li className="contact__text-item">- Atendimento personalizado</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <Partners />
          <Footer />
        </div>
      )}
    </React.Fragment>
  );
};

export default Contact;
