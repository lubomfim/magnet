import React from 'react';
import './Home.css';

import { Header } from 'components/Header';
import { Footer } from 'components/Footer';

import Hero from './hero/Hero';
import Camadas from './camadas/Camadas';
import Main from './main/Main';
import { Partners } from 'components/Partners';

const Home = (props) => {
  return (
    <React.Fragment>
      <Header />
      <Hero />
      <Camadas />
      <Main />
      <Partners />
      <Footer />
    </React.Fragment>
  );
};

export default Home;
