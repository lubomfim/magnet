import React from 'react';

import './Camadas.css';
import { CTA } from 'components/Buttons/CTA/';

const Camadas = (props) => {
  return (
    <section className="camadas">
      <div className="camadas__wrapper">
        <div className="camadas__text">
          <h2 className="camadas__title">Solução completa de CRM para sua empresa</h2>
          <p className="main__description">
            Acreditamos que o sucesso de qualquer empresa está na maneira que ela se relaciona com seus clientes
          </p>
        </div>
        <ul className="camadas__list">
          <li className="camadas__item left">
            <div className="camadas__item-content">
              <div className="camadas__item-line camadas__item-line-1">
                <p className="camadas__item-title">Primeira venda</p>
              </div>
              <p className="camadas__item-description">
                Estruture sua operação comercial e tenha total controle sobre seus resultados.
              </p>
            </div>
            <div className="camadas__item-content">
              <div className="camadas__item-line camadas__item-line-2">
                <p className="camadas__item-title">Fidelização</p>
              </div>
              <p className="camadas__item-description">
                Você precisa gerar valor em todos os momentos que o seus clientes tem contato com sua marca.
              </p>
            </div>
          </li>
          <li className="camadas__item">
            <div className="camadas__animation">
              <img src="/images/camadas/rotate-1.svg" alt="Rotate Icon" className="camadas__animation-img" />
              <div className="camadas__animation-content">
                <h2 className="camadas__animation-title">Marketing de Relacionamento</h2>
                {/*   <div className="camadas__four-balls">
                  <img
                    src="/images/camadas/ball-yellow.svg"
                    alt="Yellow Ball"
                  />
                  <img src="/images/camadas/ball-blue.svg" alt="Blue Ball" />
                  <img
                    src="/images/camadas/ball-purple.svg"
                    alt="Purple Ball"
                  />
                  <img src="/images/camadas/ball-pink.svg" alt="Pink Ball" />
                </div> */}
              </div>
            </div>
          </li>
          <li className="camadas__item right">
            <div className="camadas__item-content">
              <div className="camadas__item-line camadas__item-line-3">
                <p className="camadas__item-title">Pós-vendas</p>
              </div>
              <p className="camadas__item-description">
                Mais do que vender, você precisa entregar e gerar valor para seus clientes.
              </p>
            </div>
            <div className="camadas__item-content">
              <div className="camadas__item-line camadas__item-line-4">
                <p className="camadas__item-title">Atendimento</p>
              </div>
              <p className="camadas__item-description">
                Para entregar a melhor experiência para seus clientes, você precisa de uma visão 360º.
              </p>
            </div>
          </li>
        </ul>
      </div>
    </section>
  );
};

export default Camadas;
