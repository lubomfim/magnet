import React from 'react';
import { CTA } from 'components/Buttons/CTA/';

import './Hero.css';

const Hero = (props) => {
  return (
    <div className="hero">
      <div className="container">
        <div className="hero__wrapper">
          <h1 className="hero__title">
            Construa <span className="hero__title-span">relacionamento</span> com seus clientes e gere
            <span className="hero__title-span"> negócios</span> para sua empresa
          </h1>
          <p className="hero__description">
            A Magnet Customer é uma solução completa de construção de relacionamento com clientes.  Com ela você
            consegue organizar sua jornada comercial, acompanhar a vida dos seus clientes ao longo do tempo e ainda
            oferecer um atendimento mais inteligente e eficiente.
          </p>
          <CTA text="Quero construir relacionamento com meus clientes" fontSize="18px" padding="9px 55px" />
          <p className="hero__free-text">Vamos te ajudar nessa jornada!</p>
        </div>
      </div>
    </div>
  );
};

export default Hero;
