import React from 'react';
import { Link } from 'react-router-dom';

import './Main.css';

const Main = () => {
  return (
    <main className="main">
      <div className="container">
        <div className="main__wrapper">
          <div className="main__text">
            <h2 className="main__title">
              Vamos te ajudar a construir uma cultura de relacionamento que gera resultados para sua empresa
            </h2>
            <p className="main__description">
              Mais do que um software, CRM é união entre Pessoas, Processos e Ferramentas
            </p>
          </div>
          <div className="main__content">
            <div className="main__content-item">
              <div className="main__content-text">
                <h3 className="main__content-title">
                  Uma cultura de CRM pode gerar resultados significativos para sua empresa
                </h3>
                <p className="main__content-description">
                  Cria um ecossistema que permita seus clientes perceberem que sua empresa realmente se importa com
                  eles. Mas isso vai muito além de um software, você precisa fazer isso parte da cultura da sua empresa.
                </p>
                <Link to="/onboarding/0" className="main__content-cta">
                  Saber mais
                </Link>
              </div>
              <div className="main__content-illustration">
                <img src="/images/illustration/illustration-1.svg" alt="Dashboard" />
              </div>
            </div>
            <div className="main__content-item">
              <div className="main__content-illustration">
                <img src="/images/illustration/illustration-2.svg" alt="Dashboard" />
              </div>
              <div className="main__content-text">
                <h3 className="main__content-title">Pessoas, um dos pilares para o sucesso na cultura de CRM</h3>
                <p className="main__content-description">
                  Manter o time engajado e remando rumo aos objetivos da empresa é um desafio e tanto. Você pode tornar
                  essa caminhada um pouco mais leve se entregar para seus colaboradores a estratégia correta.
                </p>
                <Link to="/onboarding/0" className="main__content-cta">
                  Saber mais
                </Link>
              </div>
            </div>
            <div className="main__content-item">
              <div className="main__content-text">
                <h3 className="main__content-title">
                  Processos, esse pilar te permite trilhar um caminho de sucesso na cultura de CRM
                </h3>
                <p className="main__content-description">
                  “Se você não sabe para onde ir, qualquer caminho serve.” Como no diálogo entre Alice e o gato, em
                  Alice no país das Maravilhas, se você não sabe onde deseja chegar, sua empresa está perdida. Crie
                  processos que vão te ajudar a chegar do outro lado.
                </p>
                <Link to="/onboarding/0" className="main__content-cta">
                  Saiba mais
                </Link>
              </div>
              <div className="main__content-illustration">
                <img src="/images/illustration/illustration-4.svg" alt="Dashboard" />
              </div>
            </div>
            <div className="main__content-item">
              <div className="main__content-illustration">
                <img src="/images/illustration/illustration-3.svg" alt="Dashboard" />
              </div>
              <div className="main__content-text">
                <h3 className="main__content-title">
                  Ferramentas, pilar fundamental para o sucesso da cultura de CRM da sua empresa
                </h3>
                <p className="main__content-description">
                  Escolher a ferramenta de CRM correta é o maior desafio para empreendedores e empresário. Em uma
                  cultura de CRM é a união de ferramentas - gestão, financeiro, vendas, suporte e diversas outras, que
                  permitem sua empresa alcançar resultados significativos.
                </p>
                <Link to="/onboarding/0" className="main__content-cta">
                  Saber mais
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
};

export default Main;
