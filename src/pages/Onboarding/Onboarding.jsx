import React, { useEffect, useState } from 'react';
import { useHistory, useLocation, useParams } from 'react-router';
import { Link } from 'react-router-dom';

import { Timeline } from 'components/Onboarding/Timeline/';
import { FirstPage, User, Checked, Company } from 'components/Onboarding/Steps/';
import { OnboardingModal } from 'components/Onboarding/Modal';

import { saveToLocalStorage, loadFromLocalstorage, removeLocalStorage } from 'utils/localStorage';
import { ONBOARDING_TIME } from 'constants/config';
import { Mixpanel } from 'services/mixpanel/mixpanel';
import { signupUser, checkCompany, checkUser } from 'services/signup';

import './Onboarding.css';

const Onboarding = () => {
  const [register, setRegister] = useState({
    name: '',
    lastname: '',
    role: '',
    email: '',
    phone: '',
    password: '',
    confirmPassword: '',
    company: {
      name: '',
      size: '',
      segment: '',
    },
  });
  const [step, setStep] = useState(0);
  const { pathname } = useLocation();
  const { page } = useParams();
  const history = useHistory();
  const [dataSaved, setDataSaved] = useState({
    open: false,
    type: '',
  });

  useEffect(() => {
    const pageNumber = Number(page);

    if (pageNumber !== 5 && document.querySelector('.input')) {
      let primeiroInput = document.querySelector('.input');
      primeiroInput.focus();
    }

    if (pageNumber > 1 && register.name === '') {
      history.push('/onboarding/0');
    }

    setStep(pageNumber);
  }, [pathname, page, history, register.name]);

  useEffect(() => {
    const loadStorage = loadFromLocalstorage('onboarding');
    if (loadStorage) {
      let newDate = confirmTime(Date.now(), new Date(loadStorage.time).getTime(), +ONBOARDING_TIME);

      if (newDate) {
        setDataSaved({ open: false, type: '' });
        removeLocalStorage('onboarding');
        return false;
      } else {
        setDataSaved({ open: true, type: 'data' });
        return true;
      }
    } else {
      setDataSaved({ open: false, type: '' });
    }

    function confirmTime(dateNow, registerTime, minutes) {
      return dateNow > registerTime + minutes;
    }

    return () => {
      setDataSaved({ open: false, type: '' });
    };
  }, []);

  const handleUpdateOne = (type, value) => {
    setRegister({
      ...register,
      [type]: value,
    });
  };

  const handleUpdateTwo = (type, value) => {
    setRegister({
      ...register,
      company: {
        ...register.company,
        [type]: value,
      },
    });
  };

  const handleNext = async () => {
    const pageNumber = Number(page);
    const newPage = pageNumber + 1;
    switch (pageNumber) {
      case 0:
        history.push(`/onboarding/1`);
        setStep(1);
        break;
      case 2:
        if (await checkUser(register.email)) {
          handleNextPage(newPage);
        } else {
          setDataSaved({ open: true, type: 'email' });
        }
        break;
      case 3:
        if (await checkCompany(register.company.name)) {
          handleNextPage(newPage);
        } else {
          setDataSaved({ open: true, type: 'company' });
        }
        break;
      case 1 || 4:
        handleNextPage(newPage);
        break;
    }

    if (newPage === 5 && checkValue()) handleSignUp(register);
  };

  const handleNextPage = (newPage) => {
    if (checkValue()) {
      history.push(`/onboarding/${newPage}`);
      handleSaveStorage(newPage);
      setStep(newPage);
      handleMixPanel(register, newPage);
    }
  };

  const handleSaveStorage = (page) => {
    const newRegister = { ...register };
    delete newRegister.password;
    delete newRegister.confirmPassword;

    if (page <= 4) {
      saveToLocalStorage('onboarding', { register: { ...register }, step: page, time: new Date() });
    } else {
      removeLocalStorage('onboarding');
    }
  };

  const handleMixPanel = (obj, newPage) => {
    const newRegister = { ...obj };
    delete newRegister.password;
    delete newRegister.confirmPassword;

    const {
      name,
      email,
      phone,
      role,
      lastname,
      company: { companyName, size, segment },
    } = newRegister;

    const evento = {
      3: 'Iniciou cadastro',
      4: 'Informou dados da empresa',
      5: 'Concluiu cadastro',
    };

    Mixpanel.identify(email);
    Mixpanel.track(evento[newPage]);
    Mixpanel.people.set({
      $name: name + ' ' + lastname,
      $email: email,
      $phone: phone,
      $role: role,
      $company_name: companyName,
      $company_size: size,
      $company_segment: segment,
    });
  };

  const checkValue = () => {
    let allInputs = document.querySelectorAll('.input');
    let allValues = [];
    let validationPassword;

    allInputs.forEach((el) => {
      allValues.push(el);
      checkMessageError(el, 'length');

      if (Number(page) === 4) {
        validationPassword = checkPassword(el);
      }
    });

    let validationLength = allValues.every((el) => el.value.length > 0);

    if (Number(page) < 4) {
      return validationLength;
    } else if (Number(page) === 4) {
      return validationLength && validationPassword;
    }
  };

  const checkPassword = (el) => {
    const pass = register.password;

    if (pass === register.confirmPassword) {
      if (pass.match(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$ %^&*-]).{8,}$/)) {
        checkMessageError(el, 'length');
        return true;
      } else {
        checkMessageError(el, 'regex');
        return false;
      }
    } else {
      checkMessageError(el, 'match');
      return false;
    }
  };

  const checkMessageError = (el, type) => {
    let labelError = document.querySelector('.' + el.name);

    if (type === 'length') {
      if (el.value.length <= 0) {
        el.style.border = '3px solid red';

        labelError.innerHTML = 'Preencha esse campo';
        labelError.style.display = 'block';
      } else {
        el.style.border = 'none';
        labelError.style.display = 'none';
        labelError.innerHTML = 'Preencha esse campo';
      }
    } else if (type === 'regex') {
      labelError.style.display = 'block';
      labelError.innerHTML = 'Senhas não seguem os critérios';
    } else if (type === 'match') {
      labelError.style.display = 'block';
      labelError.innerHTML = 'Senhas não combinam';
    }
  };

  const handleSignUp = async (obj) => {
    const result = await signupUser(obj);
    const newPage = Number(page) + 1;
    console.log(result);
    if (result) {
      setStep(newPage);
    }
  };

  return (
    <div className="onboarding" onKeyPress={(e) => (e.key === 'Enter' ? handleNext() : '')}>
      <div className="onboarding__wrapper">
        {step > 0 && <Timeline step={step} />}
        {dataSaved.open && (
          <OnboardingModal
            setRegister={setRegister}
            setDataSaved={setDataSaved}
            dataSaved={dataSaved}
            setStep={setStep}
          />
        )}
        {step === 0 && <FirstPage handleNext={handleNext} />}
        {step > 0 && step <= 2 && (
          <User
            step={step}
            updateOne={handleUpdateOne}
            updateTwo={handleUpdateTwo}
            handleNext={handleNext}
            register={register}
          />
        )}
        {step >= 3 && step <= 4 && (
          <Company
            step={step}
            updateOne={handleUpdateOne}
            updateTwo={handleUpdateTwo}
            handleNext={handleNext}
            handlePassword={checkPassword}
            register={register}
          />
        )}
        {step >= 5 && (
          <Checked
            step={step}
            updateOne={handleUpdateOne}
            updateTwo={handleUpdateTwo}
            handleNext={handleNext}
            register={register}
          />
        )}
        <Link to="/home" className="onboarding__come-back">
          Voltar para Home
        </Link>
        <a href="mailto:contato@magnetcustomer.com" className="onboarding__mail">
          <img src="/images/onboarding/email.svg" alt="Mail Icon" />
        </a>
      </div>
    </div>
  );
};

export default Onboarding;
