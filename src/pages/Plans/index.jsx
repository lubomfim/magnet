import React from 'react';

import { Footer } from 'components/Footer';
import { Header } from 'components/Header';

import { Benefits } from 'components/Benefits';
import { FAQ } from 'components/FAQ';

import './styles.css';
import { PlansHeader } from 'components/PlansHeader';

const Plans = () => {
  return (
    <React.Fragment>
      <Header />
      <div className="plans">
        <div className="plans__wrapper">
          <PlansHeader />
          <div className="container">
            <Benefits />
            <FAQ />
          </div>
        </div>
      </div>
      <Footer />
    </React.Fragment>
  );
};

export default Plans;
