import { CTA } from 'components/Buttons/CTA';
import React from 'react';

export const SolutionFuncao = () => {
  return (
    <React.Fragment>
      <div className="main__content-item" id="vendas">
        <div className="main__content-illustration">
          <img src="/images/solution/dashboard.png" alt="Dashboard" />
        </div>
        <div className="main__content-text">
          <h2 className="about__subtitle">Vendas</h2>
          <p className="about__paragraph">
            Através do módulo comercial da Magnet Customer você vai conseguir criar processos comercias que geram
            resultados. Você pode criar suas etapas comercias, atribuir tempo limite para cada oportunidade dentro da
            etapa e ainda tem uma visualização em forma de funil.
          </p>
          <p className="about__paragraph">Inicie agora mesmo a utilização do módulo comercial da Magnet Customer</p>
          <CTA text="Iniciar agora" />
        </div>
      </div>
      <div className="main__content-item" id="marketing">
        <div className="main__content-text">
          <h2 className="about__subtitle">Marketing de relacionamento</h2>
          <p className="about__paragraph">
            Com a régua de relacionamento você consegue automatizar os processos de comunicação, definir o que
            comunicar, pra quem e quando. E ainda pode integrar aos principais canais de comunicação que sua empresa
            trabalha (E-mail, SMS, ligação, aplicativos e outros).
          </p>
          <p className="about__paragraph">
            Em alguns casos, conseguimos utilizar os dados que sua empresa já possui e criamos uma inteligência capaz de
            produzir insights valiosos para suas ações de relacionamento com clientes.
          </p>
          <p className="about__paragraph">
            Fale com um dos nossos especialista. Vamos te ajudar a estruturar a plataforma da melhor maneira para sua
            empresa
          </p>
          <CTA text="Falar com especialista" type="especialista" />
        </div>
        <div className="main__content-illustration">
          <img src="/images/solution/login.png" alt="Login magnet" />
        </div>
      </div>
      <div className="main__content-item" id="atendimento">
        <div className="main__content-illustration">
          <img src="/images/solution/produtos.png" alt="produtos manget" />
        </div>
        <div className="main__content-text">
          <h2 className="about__subtitle">Atendimento</h2>
          <p className="about__paragraph">
            Centralizamos todo histórico de atendimento dos clientes em um único lugar. Desta forma conseguimos entregar
            uma ferramenta capaz de criar unidade de informações em um ambiente de fácil utilização para o time de
            atendimento das empresas. Plataforma 100% customizável, etapas de atendimento, canais de comunicação, status
            das interações, informações dos clientes. Tudo é feito da maneira que melhor atende as necessidades da sua
            empresa
          </p>
          <p className="about__paragraph">
            Fale com um dos nossos especialista. Vamos te ajudar a construir relacionamento com seus clientes
          </p>
          <CTA text="Falar com especialista" type="especialista" />
        </div>
      </div>
      <div className="main__content-item" id="solucao">
        <div className="main__content-text">
          <h2 className="about__subtitle">Solução completa</h2>
          <p className="about__paragraph">
            Vendas – Marketing de relacionamento – Atendimento, essa é a combinação perfeita para todas empresas que
            buscam entregar uma melhor experiência de atendimento aos clientes. Com a solução completa de CRM da Magnet
            Customer sua empresa vai conseguir ter um ecossistema perfeito de construção de relacionamento com clientes.
            Dados 100% integrados, automações do inicio ao fim da jornada, histórico de relacionamento desde o processo
            comercial, estratégia de atendimento unificada.
          </p>
          <p className="about__paragraph">
            Fale com um dos nossos especialista e saiba mais como a solução completa da Magnet Customer se adequa a sua
            empresa.
          </p>
          <CTA text="Falar com especialista" type="especialista" />
        </div>
        <div className="main__content-illustration">
          <img src="/images/illustration/illustration-1.svg" alt="Dashboard" />
        </div>
      </div>
    </React.Fragment>
  );
};
