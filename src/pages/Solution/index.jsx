import { Footer } from 'components/Footer';
import { Header } from 'components/Header';
import { Partners } from 'components/Partners';
import React from 'react';
import { useParams } from 'react-router';
import { SolutionFuncao } from './funcao';
import { SolutionTamanho } from './tamanho';
import { SolutionSegment } from './segment';

import './styles.css';

const Solution = () => {
  const { assunto } = useParams();
  return (
    <React.Fragment>
      <Header />
      <div className="about">
        <div className="about-hero">
          <div className="about-hero__img">
            <img src="/images/logos/icone.png" alt="Icone Magnet" />
          </div>
        </div>
        <div className="container">
          <div className="about__wrapper">
            <section className="about__section">
              <h2 className="about__subtitle">Soluções</h2>
              <p className="about__paragraph">
                A Magnet Customer disponibiliza um ecossistema completo de soluções que vão te ajudar na construção de
                relacionamento com seus clientes. Somos uma plataforma 100% customizável e adaptável. Estamos prontos
                para atuar em com empresas de diversos tamanhos e segmento.
              </p>
            </section>
            {assunto === 'funcao' && <SolutionFuncao />}
            {assunto === 'tamanho' && <SolutionTamanho />}
            {assunto === 'segmento' && <SolutionSegment />}
          </div>
        </div>
      </div>
      <Partners />
      <Footer />
    </React.Fragment>
  );
};

export default Solution;
