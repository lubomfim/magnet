import { CTA } from 'components/Buttons/CTA';
import React from 'react';

export const SolutionSegment = () => {
  return (
    <React.Fragment>
      <div className="main__content-item" id="educacao">
        <div className="main__content-illustration">
          <img src="/images/solution/dashboard.png" alt="Dashboard" />
        </div>
        <div className="main__content-text">
          <h2 className="about__subtitle">Educação</h2>
          <p className="about__paragraph">
            Temos desenvolvido um trabalho muito positivo com diversas instituições de ensino no Brasil. Atualmente
            temos clientes atuando em diversas etapas do ciclo educacional - ensino infantil, graduação, pós-graduação,
            cursos técnicos e livres.
          </p>
          <p className="about__paragraph">
            Solicite um contato dos nossos especialistas e entenda como a Magnet Customer tem ajudado essas e outras
            empresas a construir relacionamento com seus clientes e gerar mais negócios.
          </p>
          <CTA text="Iniciar agora" />
        </div>
      </div>
      <div className="main__content-item" id="automotivo">
        <div className="main__content-text">
          <h2 className="about__subtitle">Automotivo</h2>
          <p className="about__paragraph">
            O setor automotivo foi onde tudo começou. A nossa empresa iniciou a jornada ajudando as concessionárias a
            diminuir a evasão de clientes na oficina. Sabemos que é um segmento sensível, onde os cliente tem um grau de
            exigência muito alto e geralmente não aceitam erros.
          </p>
          <p className="about__paragraph">
            Solicite um contato dos nossos especialistas e reduza a evasão de seus clientes na oficina.
          </p>
          <CTA text="Falar com especialista" type="especialista" />
        </div>
        <div className="main__content-illustration">
          <img src="/images/solution/login.png" alt="Login magnet" />
        </div>
      </div>
      <div className="main__content-item" id="servicos-financeiros">
        <div className="main__content-illustration">
          <img src="/images/solution/dashboard.png" alt="Dashboard" />
        </div>
        <div className="main__content-text">
          <h2 className="about__subtitle">Setor Financeiro</h2>
          <p className="about__paragraph">
            Entendemos que os agentes autônomos, planejadores financeiros e algumas instituições, sofrem para encontrar
            ferramentas que sejam capazes de facilitar o seu dia a dia, seja durante o processo de captação de clientes
            ou manutenção da carteira.
          </p>
          <p className="about__paragraph">
            Solicite um contato dos nossos especialistas e entenda como a Magnet Customer vai te ajudar.
          </p>
          <CTA text="Solicitar contato" type="especialista" />
        </div>
      </div>
      <div className="main__content-item" id="startups">
        <div className="main__content-text">
          <h2 className="about__subtitle">Startups</h2>
          <p className="about__paragraph">
            Temos um programa exclusivo para sua startup. Solicite o contato do nosso especialista e usufrua dos
            benefícios.
          </p>
          <CTA text="Falar com especialista" type="especialista" />
        </div>
        <div className="main__content-illustration">
          <img src="/images/illustration/illustration-1.svg" alt="Login magnet" />
        </div>
      </div>
    </React.Fragment>
  );
};
