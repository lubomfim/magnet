import { CTA } from 'components/Buttons/CTA';
import React from 'react';

export const SolutionTamanho = () => {
  return (
    <React.Fragment>
      <section className="main__content-item" id="pequena">
        <div className="main__content-illustration">
          <img src="/images/solution/dash-board.png" alt="Dashboard" />
        </div>
        <div className="main__content-text">
          <h2 className="about__subtitle">Para pequenas empresas</h2>
          <p className="about__paragraph">
            Você, pequeno empreendedor, tem o direito de oferecer o melhor para seus clientes e, é por esse motivo que a
            Magnet Customer disponibiliza um processo de implantação e utilização da plataforma em até 14 dias. Você já
            vai começar a usar a ferramenta e perceber os benefícios que uma solução de CRM gera para sua empresa.
          </p>
          <p className="about__paragraph">
            São as mesmas funcionalidades e recursos. O que diferencia uma pequena empresa de uma grande empresa é o
            tempo de implantação, pois quanto maior a empresa, mais processos, pessoas e ajustes na ferramenta.
          </p>
          <p className="about__paragraph">
            Você pode falar com um dos nossos especialistas e saber mais detalhes sobre a Magnet Customer.
          </p>
          <CTA text="Fale com o especialista" type="especialista" />
        </div>
      </section>
      <section className="main__content-item" id="grande">
        <div className="main__content-text">
          <h2 className="about__subtitle">Para grande empresas</h2>
          <p className="about__paragraph">
            Entendemos o quão sensível é uma operação de CRM em andamento. Mas também o quanto é danoso manter algo que
            não gera o resultado esperado. Geralmente grande empresas veem a área de CRM como um problema complexo de
            resolver ou algo impossível de melhorar.
          </p>
          <p className="about__paragraph">
            Aqui na Magnet Customer encaramos essas e outras situações com naturalidade, pois são mais de 15 anos
            atuando com construção de relacionamento com clientes. Nosso corpo executivo possui vasta experiência com
            projetos complexo e grandiosos, nos mais diversos setores da nossa economia.
          </p>
          <p className="about__paragraph">
            Solicite contato com o nosso Executivo Sênior. Você vai entender como fazemos a diferença.
          </p>
          <CTA text="Falar com Executivo Sênior" type="especialista" />
        </div>
        <div className="main__content-illustration">
          <img src="/images/solution/calendario.png" alt="Dashboard" />
        </div>
      </section>
    </React.Fragment>
  );
};
