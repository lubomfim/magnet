import { Input, Select } from 'components/Inputs';
import React, { useState } from 'react';
import DataSent from '../../components/sent';
import { sendSpecialist } from 'utils/sendEmail';

import './style.css';

const Specialist = () => {
  const [form, setForm] = useState({
    nome: '',
    email: '',
    role: '',
    empresa: {
      empresaNome: '',
      cidade: '',
      estado: '',
      segmento: '',
      tamanho: '',
    },
  });
  const [sent, setSent] = useState(false);

  const handleUpdateOne = (type, value) => {
    setForm({
      ...form,
      [type]: value,
    });
  };

  const handleUpdateTwo = (type, value) => {
    setForm({
      ...form,
      empresa: {
        ...form.empresa,
        [type]: value,
      },
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    let elementos = Array.from(document.querySelectorAll('.input'));
    let resposta = elementos.every((el) => el.value !== '');

    if (!resposta) {
      return;
    } else {
      try {
        await sendSpecialist(form);
        setSent(true);
        window.scrollTo(0, 0);
      } catch (err) {
        console.log(err);
      }
    }
  };

  return (
    <React.Fragment>
      {sent && <DataSent />}
      {!sent && (
        <div className="specialist">
          <div className="specialist__wrapper">
            <div className="specialist__text">
              <h2 className="specialist__title">Formulário de contato com nossos especialistas</h2>
              <p className="specialist__description">
                Os dados solicitados serão usados para construirmos sua plataforma e cadastro
              </p>
            </div>
            <div className="specialist__form-wrapper">
              <form className="specialist__form" onSubmit={(e) => handleSubmit(e)}>
                <div className="specialist__form-text">
                  <p className="specialist__form-subtitle">Sobre você</p>
                </div>
                <div className="specialist__form-half">
                  <Input
                    label="Nome *"
                    container="specialist"
                    type="text"
                    name="nome"
                    placeholder="Insira seu nome"
                    update={handleUpdateOne}
                    value={form.nome}
                    required={true}
                  />
                  <Input
                    label="E-mail *"
                    container="specialist"
                    type="email"
                    name="email"
                    placeholder="Insira seu melhor e-mail"
                    update={handleUpdateOne}
                    value={form.email}
                    required={true}
                  />
                </div>

                <div className="specialist__form-text">
                  <p className="specialist__form-subtitle">Sobre sua empresa</p>
                </div>

                <Input
                  label="Nome da empresa *"
                  container="specialist"
                  type="text"
                  name="empresaNome"
                  placeholder="Insira o nome da empresa"
                  update={handleUpdateTwo}
                  value={form.empresa.empresaNome}
                  required={true}
                />
                <Input
                  label="Seu cargo na empresa*"
                  container="specialist"
                  type="text"
                  name="role"
                  placeholder="Insira seu cargo"
                  update={handleUpdateOne}
                  value={form.role}
                  required={true}
                />
                <div className="specialist__form-half">
                  <Select
                    label="Segmento da empresa *"
                    container="specialist"
                    name="segmento"
                    value={form.empresa.segmento}
                    update={handleUpdateTwo}
                    typeData="segment"
                    required={true}
                  />
                  <Select
                    label="Quantidade de funcionários *"
                    container="specialist"
                    name="tamanho"
                    value={form.empresa.tamanho}
                    update={handleUpdateTwo}
                    typeData="size"
                    required={true}
                  />
                </div>
                <div className="specialist__form-half">
                  <Select
                    label="Estado *"
                    container="specialist"
                    name="estado"
                    value={form.empresa.estado}
                    update={handleUpdateTwo}
                    typeData="estado"
                    required={true}
                  />
                  <Select
                    label="Cidade *"
                    container="specialist"
                    name="cidade"
                    value={form.empresa.cidade}
                    update={handleUpdateTwo}
                    typeData="cidade"
                    estado={form.empresa.estado}
                    required={true}
                  />
                </div>
                <button className="specialist__button">Enviar formulário</button>
              </form>
            </div>
          </div>
        </div>
      )}
    </React.Fragment>
  );
};

export default Specialist;
