import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import { ScrollToTop } from './utils/scrollToTop';

import Home from 'pages/Home/Home';
import About from 'pages/About/About';
import Blog from 'pages/Blog/Blog';
import Contact from 'pages/Contact/Contact';
import Onboarding from 'pages/Onboarding/Onboarding';
import Solution from 'pages/Solution';
import Specialist from 'pages/Specialist';
import Plans from 'pages/Plans';

const Routes = () => (
  <BrowserRouter>
    <ScrollToTop />
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/home" component={Home} />
      <Route exact path="/sobre" component={About} />
      <Route exact path="/blog/:assunto" component={Blog} />
      <Route exact path="/solucoes/:assunto" component={Solution} />
      <Route exact path="/solucoes" component={Solution} />
      <Route exact path="/onboarding/:page" component={Onboarding} />
      <Route exact path="/especialista" component={Specialist} />
      <Route exact path="/planos" component={Plans} />
      <Route exact path="/contato" component={Contact} />
      <Route path="*" component={Home} />
    </Switch>
  </BrowserRouter>
);

export default Routes;
