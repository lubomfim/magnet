import axios from 'axios';

const URL_BASE = process.env.REACT_APP_API_BASE;

const api = axios.create({
  baseURL: URL_BASE,
});

export default api;
