import axios from 'axios';

const TOKEN_RD = process.env.REACT_APP_RD_STATION_TOKEN;
const URL_RD = process.env.REACT_APP_RD_STATION_BASE_URL;

axios.create('https://api.rd.services/auth/dialog?client_id=328823&redirect_uri={redirect_uri}');
