import api from '../api';

export const checkUser = async (email) => {
  try {
    await api.post('/check-user', { email: email });

    return true;
  } catch (error) {
    return false;
  }
};

export const checkCompany = async (fantasyName) => {
  try {
    await api.post('/check-company', { fantasyName: fantasyName });

    return true;
  } catch (error) {
    return false;
  }
};

export const signupUser = async (register) => {
  try {
    const {
      name,
      email,
      role,
      password,
      company: { size: companySize, segment, name: fantasyName },
    } = register;
    const newUser = {
      name,
      email,
      role,
      password,
      origin: 'Landing Page',
      testdrive: true,
      company: {
        fantasyName,
      },
    };

    const newCompany = {
      fantasyName,
      companySize,
      segment,
    };

    console.log(newUser, newCompany);

    const {
      data: {
        token,
        result: { _id },
      },
    } = await api.post(`/create-user`, newUser);

    api.interceptors.request.use((config) => {
      config.headers = {
        Authorization: `Bearer ${token}`,
      };
      return config;
    });

    const respCreationCompany = await api.post(`/create-starter-pack/${_id}`, newCompany);

    return true;
  } catch (err) {
    return false;
  }
};
