export const loadFromLocalstorage = (item) => {
  try {
    const serializedState = localStorage.getItem(item);

    if (serializedState === null) {
      return false;
    }
    return JSON.parse(serializedState);
  } catch (err) {
    return [];
  }
};

export const saveToLocalStorage = (item, state) => {
  try {
    const serializedState = JSON.stringify(state);
    return localStorage.setItem(item, serializedState);
  } catch (err) {
    return [];
  }
};

export const removeLocalStorage = (item) => {
  try {
    localStorage.removeItem(item);
  } catch (err) {
    return [];
  }
};
