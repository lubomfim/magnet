import axios from 'axios';

export const sendSpecialist = async ({
  nome: name,
  email,
  role,
  empresa: { segmento: segment, estado: state, cidade: city, empresaNome: company, tamanho: employees },
}) => {
  const data = {
    name,
    employees,
    email,
    role,
    company,
    segment,
    state,
    city,
  };

  const url = 'https://y8oxzxot8j.execute-api.us-east-1.amazonaws.com/prod/email';

  try {
    const response = await axios.post(url, JSON.stringify(data), {
      headers: {
        'Content-Type': 'application/json',
      },
    });

    return response;
  } catch (err) {
    return err;
  }
};

export const sendContact = async ({ nome: name, email, empresa: company, mesagem: message }) => {
  const data = {
    name,
    email,
    company,
    message,
  };

  const url = 'https://iuuysm2n6c.execute-api.us-east-1.amazonaws.com/prod/email';

  try {
    const response = await axios.post(url, JSON.stringify(data), {
      headers: {
        'Content-Type': 'application/json',
      },
    });

    return response;
  } catch (err) {
    return err;
  }
};
